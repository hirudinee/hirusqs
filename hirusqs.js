let AWS = require('aws-sdk');
const sqs = new AWS.SQS();

exports.handler = async (event) => {
    // try {
    //     let data = await sqs.receiveMessage({
    //         QueueUrl: `https://sqs.${process.env.AWS_REGION}.amazonaws.com/${process.env.SIGMA_AWS_ACC_ID}/KTestSQS`,
    //         MaxNumberOfMessages: 1,
    //         VisibilityTimeout: 30,
    //         WaitTimeSeconds: 0,
    //         AttributeNames: ['All']
    //     }).promise();

    //     //console.log("receive "+data);
    //     //sample = data.Message.ReceiptHandle
    //     console.log(sample)

    // } catch (err) {
    //     // error handling goes here
    // };


    try {
        let sample = await sqs.deleteMessage({
            QueueUrl: `https://sqs.${process.env.AWS_REGION}.amazonaws.com/${process.env.SIGMA_AWS_ACC_ID}/KTestSQS`,
            ReceiptHandle: sample
        }).promise();

        console.log("delete "+sample)

    } catch (err) {
        // error handling goes here
    };

    //return { "message": "Successfully executed" };
};